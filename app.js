// INITIAL UI STATE
function start(filename) {
    loadData(filename + '.txt', parseTelemetryDataFile);
}

// ADD NEW FILE
function newFile() {
    let input = document.getElementById("userInput").value.replace(/\.[^/.]+$/, "");
        loadData(input + '.txt', parseTelemetryDataFile);
}

// LOAD THE TELEMETRY DATA FILE
function loadData(url, callback) {
    let request = new XMLHttpRequest();
    request.onreadystatechange = function()
    {
        if (request.readyState == 4 && request.status != 200) {
            alert("Sorry, your file can't be found. \nPossible causes:\n1. There is a spelling error\n2. The file does not exist in the root directory")
        } else if (request.readyState == 4 && request.status == 200) {
            callback(request.responseText);
        }
    };
        request.open('GET', url);
        request.send();
}

// READ AND PARSE THE TELEMETRY DATA FILE
function parseTelemetryDataFile(data) {
    splitData = data.split('\n');
    splitData.pop();
    const parseTelemetryData = splitData.map((val, i, arr) => {
        return splitData[i].split('|');
    });
    createTelemetryObject(parseTelemetryData);
}

// CREATE THE TELEMETRY OBJECT
function createTelemetryObject(parseTelemetryData) {
    const trendLength = 5 * 60 * 1000;
    const trendAlertSetting = 3;
    const lastLowBatteryTrendStartTime = {};
    const lastLowBatteryTrendLastTime = {};
    const lastHighThermostatTrendStartTime = {};
    const lastHighThermostatTrendLastTime = {};
    const lowBattLastTrendCount = {};
    const highThermostatLastTrendCount = {};

    const telemetryObjectKeys = [
        'timestamp',
        'satelliteId',
        'red-high-limit',
        'red-low-limit',
        'yellow-high-limit',
        'yellow-low-limit',
        'raw-value',
        'component'
    ];
    const createdTelemetryObject = parseTelemetryData.map(function(obj) {
        const telemetryObject = {};
        for (i = 0; i < obj.length; i++) {
            telemetryObject[telemetryObjectKeys[i]] = obj[i];
        }

        const current = parseFloat(telemetryObject['raw-value']);
        const red_high = parseFloat(telemetryObject['red-high-limit']);
        const red_low = parseFloat(telemetryObject['red-low-limit']);
        const satId = parseInt(telemetryObject['satelliteId']);
        const dtgRaw = telemetryObject['timestamp'];
        const reggie = /(\d{4})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2})\.(\d{3})/;
        const dateArray = reggie.exec(dtgRaw);
        const newString =
            dateArray[1] +
            '-' +
            dateArray[2] +
            '-' +
            dateArray[3] +
            'T' +
            dateArray[4] +
            ':' +
            dateArray[5] +
            ':' +
            dateArray[6] +
            '.' +
            dateArray[7] +
            'Z';
        const dtg = new Date(newString);
        telemetryObject['timestamp'] = dtg.toISOString();
        const comp = telemetryObject['component'];
        let alert_string = 'unset';

        // TRACK THE THERMOSTAT CONDITION
        if (current > red_high && comp == 'TSTAT') {
            let lastTrendStart = Date.parse('01 Jan 1970 00:00:00 GMT');
            if (lastHighThermostatTrendLastTime[satId] != null) {
                lastTrendStart = lastHighThermostatTrendLastTime[satId];
            }
            const diff = dtg - lastTrendStart;
            if (diff <= trendLength) {
                highThermostatLastTrendCount[satId] += 1;
            } else {
                highThermostatLastTrendCount[satId] = 1;
                lastHighThermostatTrendStartTime[satId] = dtg;
            }
            lastHighThermostatTrendLastTime[satId] = dtg;
            if (highThermostatLastTrendCount[satId] >= trendAlertSetting) {
                alert_string = 'RED HIGH';
                telemetryObject['timestamp'] =
                    lastHighThermostatTrendStartTime[satId];
            }
        }

        // TRACK THE BATTERY CONDITION
        if (current < red_low && comp == 'BATT') {
            let lastTrendStart = Date.parse('01 Jan 1970 00:00:00 GMT');
            if (lastLowBatteryTrendLastTime[satId] != null) {
                lastTrendStart = lastLowBatteryTrendLastTime[satId];
            }
            const diff = dtg - lastTrendStart;
            if (diff <= trendLength) {
                lowBattLastTrendCount[satId] += 1;
            } else {
                lowBattLastTrendCount[satId] = 1;
                lastLowBatteryTrendStartTime[satId] = dtg;
            }
            lastLowBatteryTrendLastTime[satId] = dtg;
            if (lowBattLastTrendCount[satId] >= trendAlertSetting) {
                alert_string = 'RED LOW';
                telemetryObject['timestamp'] =
                    lastLowBatteryTrendStartTime[satId];
            }
        }

        if (alert_string != 'unset') {
            telemetryObject.severity = alert_string;
        }
        return telemetryObject;
    });

    jsonOutput(createdTelemetryObject);
}

// HANDLE ("KEY" === "VALUE") CHANGES
function replacer(key, value) {
    if (key === 'red-high-limit') return undefined;
    else if (key === 'red-low-limit') return undefined;
    else if (key === 'yellow-high-limit') return undefined;
    else if (key === 'yellow-low-limit') return undefined;
    else if (key === 'raw-value') return undefined;
    else if (key === 'satelliteId') return parseInt(value);
    else return value;
}

// OUTPUT TO SCREEN
function jsonOutput(createdTelemetryObject) {
    const jsonTelemetryObjectOutputFilter = createdTelemetryObject.filter(
        row => row['severity'] != null
    );
    const jsonTelemetryObjectOutput = JSON.stringify(jsonTelemetryObjectOutputFilter, replacer, 2);
    console.log(`JSON output: ${jsonTelemetryObjectOutput}`);

    const telemetryTable = jsonTelemetryObjectOutputFilter
        .sort(function(a, b) {
            return b['timestamp'] - a['timestamp'];
        })
        .map(function(satData) {
            if (satData['component'] == 'BATT') {
                return `
                <tr id="telemetry__body">
                    <td class="satelliteId">${satData['satelliteId']}</td>
                    <td class="component">Battery</td>
                    <td class="timestamp">${satData['timestamp']}</td>
                    <td class="severity"><span class="alert">${satData['severity']}</span></td>
                </td>
                `;
            } else {
                return `
                <tr id="telemetry__body">
                    <td class="satelliteId">${satData['satelliteId']}</td>
                    <td class="component">Thermostat</td>
                    <td class="timestamp">${satData['timestamp']}</td>
                    <td class="severity"><span class="alert">${satData['severity']}</span></td>
                </td>
                `;
            }
        })
        .join('');
    document.querySelector('#telemetry__table tbody').innerHTML = telemetryTable;
    document.querySelector('#render__json').value = jsonTelemetryObjectOutput;
}
