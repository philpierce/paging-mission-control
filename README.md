# PAGING MISSION CONTROL (Enlighten Coding Challenge)

## RENDERING INSTRUCTIONS

1. The User Interface can be rendered in any modern browser utilizing a web server accessing localhost.
2. The User Interface initially ingests the file ```(data.txt)``` from the local root directory.
    - To change the file ingested, copy and paste your new file into the root directory.
    - Enter the files name in the input field under the heading __TELEMETRY FILE INPUT__.
    - Press __SUBMIT__.

## THE VIEW

### SATELLITE HEALTH TABLE

 1. Renders alert events sorted by latest violation condition in a descending order.
 2. Renders a human readable Date Time Group of each violation condition based on Users Time Zone.
 3. Auto scrolls when rendering large data files, maintaining a single page aspect for a better UX.

### JSON RENDER

 1. The JSON Render view shows the JSON in its native format.
 2. Auto scrolls when rendering large JSON blobs.

### TELEMETRY FILE INPUT

 1. The input area accepts the filename of any appropriately loaded ```.txt``` file.
 2. The __SUBMIT__ button will call the file and render it to screen
 3. The __CLEAR__ button will refresh the browser to its initial state.

## DECISIONS

 1. As a time sensitive problem solving exercise, the project was executed in a language I am more comfortable with (JavaScript). However, Python is probably a better choice for this exercise, and I would try that route if I had more time.
 2. This was executed using only standard JavaScript (no tooling or frameworks) with minor html and CSS styling for a unified User Interface.

## ASSUMPTIONS

 1. As a Front-End Developer, my assumption is that this challenge (as a project) would ultimately become a UI, therefore I geared my concept and ideas toward that end.
 2. The JSON output in my solution differs in order, but not content, than the supplied example.  I did not address this because, by definition, JSON is an un-ordered list of keys and their associated values. Since the content matches, I assumed it satisfies requirement.  If exact ordering was required for some reason, I would add code and spend time to do this.
 3. Some thought has been given to the idea of "continuous alerting":
    - If a satellite's thermostat or battery violates conditions it will alert the main satellite health table (and Json console). However, if another violation condition happens that "connects" two (or more) five minute trends, the UI will alert again. So, the user will see:
         a. When the intial alert happened.
         b. An additional alert for each consecutive 5min alerting period.


